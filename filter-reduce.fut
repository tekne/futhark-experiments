let sum_pos (xs: []i32) = reduce (+) 0 (filter (>0) xs)
let sum_pos_better (xs: []i32) = reduce (+) 0 (map (\x -> if x > 0 then x else 0) xs)
let reduce_some 'a (op: a -> a -> a) (ne: a) (p: a -> bool) (xs: []a): a =
    reduce op ne (map (\x -> if p x then x else ne) xs)
let sum_pos_best (xs: []i32) =
    reduce_some (+) 0 (>0) xs

type with_neutral 't = #neutral | #val t
let f_with_neutral 't (f: t -> t -> t) (x: with_neutral t) (y: with_neutral t): with_neutral t =
    match (x, y)
    case (#val x, #val y) -> #val (f x y)
    case (#neutral, _) -> y
    case (_, #neutral) -> x

let reduce1 't (f: t -> t -> t) (ts: []t) : with_neutral t =
    reduce (f_with_neutral f) #neutral (map (\t -> #val t) ts)