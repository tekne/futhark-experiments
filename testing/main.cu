#include <stdio.h>
#include <cuda.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>

#include "kernels.h"
#include "matmul.h"

#define FLOAT_ERR 0.001

float random_float() {
    float multiplier = 1.0;
    return (float)rand()/(float)(RAND_MAX) * multiplier;
}

bool verify(float* actual, float* expect, int h, int w) {
    // printf("%f %f\n", actual[0], actual[1]);
    // printf("%f %f\n", expect[0], expect[1]);
    gsl_matrix_float_view actual_gsl = gsl_matrix_float_view_array(actual, h, w);
    gsl_matrix_float_view expect_gsl = gsl_matrix_float_view_array(expect, h, w);
    gsl_matrix_float_sub(&expect_gsl.matrix, &actual_gsl.matrix);
    float min_v, max_v;
    gsl_matrix_float_minmax(&expect_gsl.matrix, &min_v, &max_v);
    if (abs(min_v) > FLOAT_ERR || abs(max_v) > FLOAT_ERR) {
        fprintf(stderr, "ERROR, %f, %f\n", abs(min_v), abs(max_v));
        return false;
    }
    return true;
}

void cpu_matmul(float* a, float* b, float* c, int N, int M, int Q) {
    gsl_matrix_float_view a_gsl = gsl_matrix_float_view_array(a, N, M);
    gsl_matrix_float_view b_gsl = gsl_matrix_float_view_array(b, M, Q);
    gsl_matrix_float_view c_gsl = gsl_matrix_float_view_array(c, N, Q);
    gsl_blas_sgemm(CblasNoTrans, CblasNoTrans, 1.0, &a_gsl.matrix, &b_gsl.matrix, 0.0, &c_gsl.matrix);
}


int main() {
    srand(time(0));

    int N1 = 64;
    int N2 = 128;
    // int N3 = ;
    int M = 768;
    int Q = 2304;

    float* A;
    float* B;
    float* result1;
    float* a_cpu;
    float* b_cpu;
    float* res1;
    float* actual1;

    float* C;
    float* D;
    float* result2;
    float* c_cpu;
    float* d_cpu;
    float* res2;
    float* actual2;

    a_cpu = (float*)malloc(N1 * M * sizeof(float));
    b_cpu = (float*)malloc(M * Q * sizeof(float));
    res1 = (float*)malloc(N1* Q * sizeof(float));
    actual1 = (float*)malloc(N1 * Q * sizeof(float));

    c_cpu = (float*)malloc(N2 * M * sizeof(float));
    d_cpu = (float*)malloc(M * Q * sizeof(float));
    res2 = (float*)malloc(N2 * Q * sizeof(float));
    actual2 = (float*)malloc(N2 * Q * sizeof(float));

    for (int i = 0; i < N1 * M; i++) {
        a_cpu[i] = random_float();
    }
    for (int i = 0; i < N2 * M; i++) {
        c_cpu[i] = random_float();
    }
    for (int i = 0; i < M * Q; i++) {
        b_cpu[i] = random_float();
        d_cpu[i] = random_float();
    }
    cpu_matmul(a_cpu, b_cpu, res1, N1, M, Q);
    cpu_matmul(c_cpu, d_cpu, res2, N2, M, Q);

    struct futhark_context_config* futhark_cfg = futhark_context_config_new();
    struct futhark_context* ctx = futhark_context_new(futhark_cfg);
    struct futhark_f32_2d* a_f = futhark_new_f32_2d(ctx, a_cpu, N1, M);
    struct futhark_f32_2d* b_f = futhark_new_f32_2d(ctx, b_cpu, M, Q);

    struct futhark_f32_2d* res_f;
    futhark_entry_matmul_f32(ctx, &res_f, a_f, b_f);



    if (cudaMalloc(&A, N1 * M * sizeof(float)) != cudaSuccess) {
        fprintf(stderr, "cudaMalloc A failed\n");
    }
    if (cudaMalloc(&B, M * Q * sizeof(float)) != cudaSuccess) {
        fprintf(stderr, "cudaMalloc B failed\n");
    }
    if (cudaMalloc(&result1, N1 * Q * sizeof(float)) != cudaSuccess) {
        fprintf(stderr, "cudaMalloc result failed\n");
    }
    cudaMemcpy(A, a_cpu, N1*M*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(B, b_cpu, M*Q*sizeof(float), cudaMemcpyHostToDevice);
    
    if (cudaMalloc(&C, N2 * M * sizeof(float)) != cudaSuccess) {
        fprintf(stderr, "cudaMalloc A failed\n");
    }
    if (cudaMalloc(&D, M * Q * sizeof(float)) != cudaSuccess) {
        fprintf(stderr, "cudaMalloc B failed\n");
    }
    if (cudaMalloc(&result2, N2 * Q * sizeof(float)) != cudaSuccess) {
        fprintf(stderr, "cudaMalloc result failed\n");
    }
    cudaMemcpy(C, c_cpu, N2*M*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(D, d_cpu, M*Q*sizeof(float), cudaMemcpyHostToDevice);

    fuse<<<432, 32>>>(A, B, C, D, result1, result2);
    cudaDeviceSynchronize();
    cudaMemcpy(actual1, result1, sizeof(float)*N1 * Q,cudaMemcpyDeviceToHost);
    cudaMemcpy(actual2, result2, sizeof(float)*N2 * Q,cudaMemcpyDeviceToHost);
    // cudaMemcpy(actual3, result3, sizeof(float)*N3 * Q,cudaMemcpyDeviceToHost);
    verify(actual1, res1, N1, Q);
    printf("+++++++++++++\n");
    verify(actual2, res2, N2, Q);
    // verify(actual3, res3, N3, Q);
    // verify(A, B, result1, N1, M, Q);
    // verify(C, D, result2, N2, M, Q);

    printf("Start profiling......\n");
    
    // cudaEvent_t start;
    // cudaEvent_t stop;
    // float total = 0;
    // cudaEventCreate(&start);
    // cudaEventCreate(&stop);
    // cudaEventRecord(start);
    // for (int i = 0; i < 100; i++) {
    //     fuse<<<1500, 64>>>(A, B, C, D, result1, result2);
    // }
    // cudaEventRecord(stop);
    // cudaEventSynchronize(stop);
    // float milliseconds = 0;
    // cudaEventElapsedTime(&milliseconds, start, stop);
    // total += milliseconds;
    // printf("%f\n", total / 100.0);
    
    cudaEvent_t start;
    cudaEvent_t stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    double data[100];
    float milliseconds = 0;
    for (int i = 0; i < 105; i++) {
        cudaEventRecord(start);
    
        futhark_entry_matmul_f32(ctx, &res_f, a_f, b_f);
    
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&milliseconds, start, stop);
        if (i >= 5) {
            data[i - 5] = (double)milliseconds * 1000.0;
        }
    }
    double mean = gsl_stats_mean(data, 1, 100);
    double variance = gsl_stats_variance(data, 1, 100);

    printf("The mean is %g, while the variance is %g \n", mean, variance);
    printf("And the first 10 data sample is: \n");
    for (int i = 0; i < 10; i++) {
        printf("%g ", data[i]);
    }
    printf("\n");


    cudaFree(A);
    cudaFree(B);
    cudaFree(result1);
    cudaFree(result2);
    
    
}