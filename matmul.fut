import "lib/github.com/diku-dk/linalg/linalg"

module linalg_f32 = mk_linalg f32

entry matmul_f32 = linalg_f32.matmul

let fixed_matmul 'a
           (add: a -> a -> a) (mul: a -> a -> a) (zero: a)
           (A: [64][768]a) (B: [768][2304]a) : [64][2304]a =
  map (\A_row ->
         map (\B_col ->
                reduce add zero (map2 mul A_row B_col))
             (transpose B))
      A

entry fixed_matmul_f32 = fixed_matmul (+) (*) 0f32
